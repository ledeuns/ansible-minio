# ansible-minio

Ansible role to deploy MinIO infrastructure

### Certificate generation

Certificates are generated with `generate_cert.go` (https://golang.org/src/crypto/tls/generate_cert.go) :

```
go run generate_cert.go -ca --host "192.168.255.51,minio1"; mv {cert.pem,key.pem} files/minio1
```

### Get binaries

Get MinIO binaries from the official website to push them on the nodes :

```
ftp -o files/binaries/minio https://dl.min.io/server/minio/release/linux-amd64/minio
ftp -o files/binaries/mc https://dl.min.io/client/mc/release/linux-amd64/mc
```
